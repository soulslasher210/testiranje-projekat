/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.Client;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Blizz
 */
public class ClientServiceTest {
    
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    public ClientServiceTest() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Treba da prolazi posto vraca null a ovakav user ne bi trebao da postoji 
     * barem ne u prvom slucaju posto je u bazi moguce kreirati praznog usera
     * ako se ponovo pokrene postojace ovakav user i onda ovaj expResult null
     * ce biti netacan mislim znam da ne bi trebalo da se pokrene dva puta ali
     * samo dajem ovde nagovestaj na to
     */
    @Test
    public void testLogin() {
        System.out.println("Bez username i imena");
        String username = "";
        String password = "";
        ClientService instance = new ClientService();
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
       
       
    }
    /*Ovde ocekujem da mi vrati null */
       @Test
    public void testLogin8() {
        System.out.println("Bez postojeceg imena i password");
        String username = "_";
        String password = "_";
        ClientService instance = new ClientService();
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
       
       
    }
    //Treba da prodje posto postoje ovaj username i password
     @Test
    public void testLogin2() {
        System.out.println("Sa postojecim username i password");
        String username = "do1";
        String password = "do2";
        ClientService instance = new ClientService();
        Client a = new Client(6,"Dom","do1","do2");
        Client result = instance.login(username, password);
        assertEquals(a.getUsername(), result.getUsername());
         assertEquals(a.getPassword(), result.getPassword());
       
       
    }
    /*Ako dobro razumem ovde je bug posto trebaju oba polja da budu postojeca
   sto znaci da se user moze login samo sa username GRESKA*/
     @Test
    public void testLogin3() {
        System.out.println("Sa postojecim username bez password");
        String username = "ti1";
        String password = "";
        ClientService instance = new ClientService();
       Client a = new Client(1,"Titania","ti1","ti2");
        Client result = instance.login(username, password);
        assertEquals(a.getPassword(), result.getPassword());
       
       
    }
    /*isto tako i u ovom slucaju posto vraca model nazad sto znaci da je nasao
    ovakav password u bazi ali nema username GRESKA*/
    @Test
    public void testLogin4() {
        System.out.println("Sa postojecim password bez username");
        String username = "";
        String password = "t2";
        ClientService instance = new ClientService();
       Client a = new Client(7,"Test","t1","t2");
        Client result = instance.login(username, password);
        assertEquals(a.getUsername(), result.getUsername());
       
    }
    //Prolazi pošto ovakav korisnik ne postoji u bazi
      @Test
    public void testLogin5() {
        System.out.println("Sa nepostojecim password i username");
        String username = "asddsa";
        String password = "aleasdasks9";
        ClientService instance = new ClientService();
        Client a = new Client(2,"Mark","mark","mark");
        Client result = instance.login(username, password);
        assertEquals(a.getPassword(), result.getPassword());
        assertEquals(a.getUsername(), result.getUsername());
       
    }
    
    /*Trebalo bi da bude null pointer exception error što znači da ne postoji ovakav korisnik
    ,ali pošto je moguće kreirati praznog korisnika ovaj test može da se poništi u tom slučaju*/
     @Test
    public void testLogin6() {
        System.out.println("Sa praznim username a popunjenuim password ");
       String username = "";
        String password = "aleasdasks9";
        ClientService instance = new ClientService();
        Client a = new Client(3,"Oberon","ob1","ob2");
        Client result = instance.login(username, password);
        assertEquals(a.getPassword(), result.getPassword());
    }
    
    
     //Trebalo bi da bude null pointer exception error što znači da ne postoji ovakav korisnik
    @Test
    public void testLogin7() {
        System.out.println("Sa praznim password a popunjenuim username(nepostojeci) ");
        String username = "aleasdasks9";
        String password = "";
        ClientService instance = new ClientService();
        Client a = new Client(4, "Titania", "ti1", "ti2");
        Client result = instance.login(username, password);
        assertEquals(a.getPassword(), result.getPassword());
    }
       
       
    /**
     * Test of register method, of class ClientService.
     */
    
    
    //Kaze da su sva polja obavezna ali kreira ovakvog usera u bazi GRESKA
     @Test
    public void testRegister() {
        System.out.println("register");
        String name = "";
        String username = "";
        String password = "";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    /*Treba da prolazi ako ovakav user ne postoji,ali ako postoji on ponovo prolazi GRESKA*/
      @Test
    public void testRegister2() {
        System.out.println("register");
        String name = "Mark";
        String username = "IDiDnOtHitHer";
        String password = "oHeyMark";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
   //Trebalo bi da prolazi ne kreira ovakvog usera posto mu nisu popunjena sva polja
    //U bazi ne kreira zato sto nema username
     @Test
    public void testRegister3() {
        System.out.println("register");
        String name = "Polo";
        String username = "";
        String password = "Marko";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    //Ovde ga kreira ako ima username ?Greska
     @Test
    public void testRegister4() {
        System.out.println("register sa imenom i username");
        String name = "Polo";
        String username = "Marko";
        String password = "";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
     //Ovde ga kreira ako ima username 
     @Test
    public void testRegister5() {
        System.out.println("register samo sa username");
        String name = "";
        String username = "PoloVOlkswagen";
        String password = "";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
     //Ako postoji samo username kreirace novog korisnika u tabeli sto nema smisla GRESKA
     @Test
    public void testRegister6() {
        System.out.println("register samo sa password");
        String name = "";
        String username = "";
        String password = "pass";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    
    
    
    

    /**
     * Test of deleteUser method, of class ClientService.
     */
    
    
    //Prolazi u tabeli ako nepostoji vratice false,ako se doda onda ce da vrati true
    @Test
    public void testDeleteUser() {
        System.out.println("deleteUser");
        Client c = new Client(5,"Loki","Lo1","Lo2");
        ClientService instance = new ClientService();
        boolean expResult = true;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
    }
    
    
    
     //Izbacuje null pointer ako mu nije zadat id
    @Test
    public void testDeleteUser2() {
        System.out.println("deleteUser2");
        Client c = new Client(null,"Aleksandar","aleks100","aleks9");
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
    }
    
    //Brisanje nepostojeceg usera treba da prodje
     @Test
    public void testDeleteUser3() {
        System.out.println("deleteUser2");
        Client c = new Client(15,"Aleksandar","as","as1");
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateInfo method, of class ClientService.
     */
    
    
    //update se samo password ime ne GRESKA
    @Test
    public void testUpdateInfo() {
        System.out.println("updateInfo");
        Client c = new Client(1, "Aleksandar", "aleks100", "aleks9");
        String name = "Soulslasher210";
        String oldPassword = "aleks9";
        String password = "aleks100";
        ClientService instance = new ClientService();
        instance.updateInfo(c, name, oldPassword, password);
        assertEquals(c.getPassword(), password);
        assertEquals(c.getName(), name);
        
    }
    
    
    //Treba da padne posto ne postoji ovakav user GRESKA
    @Test
    public void testUpdateInfo2() {
        System.out.println("updateInfo");
        Client c = new Client(87, "Doms", "do1", "do2");
        String name = "Soulslasher210";
        String oldPassword = "aleks9";
        String password = "aleks100";
        ClientService instance = new ClientService();
        instance.updateInfo(c, name, oldPassword, password);
        assertEquals(c.getPassword(), password);
        assertEquals(c.getName(), name);
        
       
    }

   
    
}
