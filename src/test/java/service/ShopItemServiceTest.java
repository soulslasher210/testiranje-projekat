/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.ShopItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Blizz
 */
public class ShopItemServiceTest {
    
    public ShopItemServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of postItem method, of class ShopItemService.
     */
    
    
    //Ovo treba da prodje posto je sve uneto
    @Test
    public void testPostItem() {
        System.out.println("postItem");
        String name = "Papir";
        float price = 6;
        int amount = 3;
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
    }
    //Ovo je false
     @Test
    public void testPostItem2() {
        System.out.println("bezImena");
        String name = "";
        float price = 3;
        int amount = 2;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
    }
    //Bilo sta drugo ako nije u parametrima kojima je trazeno nece biti uneto
       @Test
    public void testPostItem3() {
        System.out.println("bezImena");
        String name = "";
        float price = 0.0f ;
        int amount = 0;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
    }
    
    
      @Test
    public void testPostItem4() {
        System.out.println("negativniBrojevi");
        String name = "Viljamovka";
        float price = -1 ;
        int amount = -2;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of removeItem method, of class ShopItemService.
     */
    
    //Uvek je true iako nepostoji ovaj "Hleb sa ovim id" GRESKA mozda?
    @Test
    public void testRemoveItem() {
        System.out.println("removeItem");
        ShopItem s = new ShopItem(5, "Nesto", 13, 120);
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
       
    }
    
      @Test
    public void testRemoveItem2() {
        System.out.println("removeItem Null");
        ShopItem s = new ShopItem(null, "Hleb", 6, 3);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of buy method, of class ShopItemService.
     */
    
    //Ne belezi u bazi podataka, ne smanjuje se vrednost u tabeli 
    @Test
    public void testBuy() {
        System.out.println("buy");
        ShopItem s = new ShopItem(9, "Moka", 3, 5);
        int amount = 3;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        assertEquals(s.getAmount(), 2);
      
    }
    
      public void testBuy2() {
        System.out.println("buy");
        ShopItem s = new ShopItem(4, "Voda", 2, 200);
        int amount = -5;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
        assertEquals(s.getAmount(), 195);
      
    }

   /**
     * testStockUp ce proci, pisace da je negativan broj ali ce se  updatovati posto ce samo ici return if  jer nema false i true
     * trebalo bi da spreci negativni broj GRESKA?
     */
      @Test
    public void testStockUp() {
        System.out.println("stockUp");
        ShopItem s = new ShopItem(6,"Hleb",5,7);
        int amount = -500;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        assertEquals(amount, s.getAmount());
    }
    //Ovaj test ce proci bez greske jer nije negativan broj
    @Test
    public void testStockUp2() {
        System.out.println("stockUp");
        ShopItem s = new ShopItem(7,"Majonez",3,6);
        int amount = 500;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        assertEquals(amount, s.getAmount());
    }

    /**
     * Ovaj test ce uvek biti true u kodu ne moze da bude false osim ako se id ne postoji
     * Greska?
     */
    
    @Test
    public void testCheckIfPopular() {
        System.out.println("checkIfPopular");
        ShopItem s = new ShopItem(8, "Kupus", 1, 5);
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
      
    }
      @Test
    public void testCheckIfPopular2() {
        System.out.println("checkIfPopular2");
        ShopItem s = new ShopItem(23, "Cvekla", 2, 4);
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
      
    }

//Rezultat je uvek nula bez obzira da li se indeks podudara ili ne
    @Test
    public void testGetTrendingIndex() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(1,"Viski",5,100);
        ShopItemService instance = new ShopItemService();
        float expResult = 152;//stavljao sam bilo sta da proverim da li valja uopste
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result,0.1);
    }
    //testGetTrendingIndex2 id se ne podudara onda je indeks na kraju 0
    @Test
    public void testGetTrendingIndex2() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(13,"Voda",3,5);
        ShopItemService instance = new ShopItemService();
        float expResult = 0;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.1);
    }
    
    //testGetTrendingIndex3 id ne postoji onda je se izbacuje IllegalArgumentException
    @Test
    public void testGetTrendingIndex3() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(null,"Vodka",75,10);
        ShopItemService instance = new ShopItemService();
        float expResult = 0;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.1);
    }
    
    //Kraj testiranja GetTrendingIndex
    
}

