/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;


import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Blizz
 */
public class PromotionServiceTest {
    
    public PromotionServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of calculateSeasonalPromotions method, of class PromotionService.
     */
    
    
     //Ovaj ceo test ne pravilan posto  ne postoji promotion u bazi podataka i kod je  nepostojeci
    @Test
    public void testCalculateSeasonalPromotions() {
        System.out.println("calculateSeasonalPromotions");
        Date d = new Date() ;
        PromotionService instance = new PromotionService();
        instance.calculateSeasonalPromotions(d);
        
      
    }

    /**
     * Test of checkHolidays method, of class PromotionService.
     */
    
    //Uvek vraca false
    @Test
    public void testCheckHolidays() {
        System.out.println("checkHolidays");
        Date d = new Date();
        PromotionService instance = new PromotionService();
        boolean expResult = false;
        boolean result = instance.checkHolidays(d);
        assertEquals(expResult, result);
    }

    /**
     * ne radi nista
     */
    @Test
    public void testCalculateSpecialPromotions() {
        System.out.println("calculateSpecialPromotions");
        PromotionService instance = new PromotionService();
        instance.calculateSpecialPromotions();
        
    }

    /**
     * Test of calculateDiscount method, of class PromotionService.
     */
    
    // uvek stavlja popust na 1000
    @Test
    public void testCalculateDiscount() {
        System.out.println("calculateDiscount");
        PromotionService instance = new PromotionService();
        instance.calculateDiscount();
       
    }

    /**
     * koristi metod koji ne radi nista
     */
    @Test
    public void testApplyPromotions() {
        System.out.println("applyPromotions");
        float price = 100;
        Date d = new Date();
        PromotionService instance = new PromotionService();
        float expResult = 1000;
        float result = instance.applyPromotions(price, d);
        assertEquals(expResult, result, 0.1);
       
    }
    
}
