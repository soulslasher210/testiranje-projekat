/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.DeliveryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Blizz
 */
public class DeliveryServiceServiceTest {
    
    public DeliveryServiceServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of register method, of class DeliveryServiceService.
     */
    
    //Nista nije uneto
    @Test
    public void testRegister() {
        System.out.println("register");
        String name = "";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
//samo ime
    @Test
    public void testRegister2() {
        System.out.println("register2");
        String name = "Djeneral";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
    
    
    //uneto sve treba da prodje 
    @Test
    public void testRegister3() {
        System.out.println("register2");
        String name = "FED EX";
        float pricePerKilometer = 2;
        float startingPrice = 3;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
    //samo cena po km
    @Test
    public void testRegister4() {
        System.out.println("register2");
        String name = "";
        float pricePerKilometer = 7;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
    
      //samo pocetna cena 
    @Test
    public void testRegister5() {
        System.out.println("register2");
        String name = "";
        float pricePerKilometer = 0.0F;
        float startingPrice = 1;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
    
    
    //Ime i pocetna cena
    @Test
    public void testRegister6() {
        System.out.println("register2");
        String name = "Maxis";
        float pricePerKilometer = 0.0F;
        float startingPrice = 6;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
    
    //Ime i cena po km
    @Test
    public void testRegister7() {
        System.out.println("register2");
        String name = "Maxis";
        float pricePerKilometer =5;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
     //samo cena po km i pocetna cena
    @Test
    public void testRegister8() {
        System.out.println("register2");
        String name = "";
        float pricePerKilometer =2;
        float startingPrice = 3;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
       
    }
    
    
    
    /**
     * Test of deleteDeliveryService method, of class DeliveryServiceService.
     */
    
    
    
    @Test
    public void testDeleteDeliveryService() {
        System.out.println("deleteDeliveryService");
        DeliveryService d = new DeliveryService(1, "Numbian", 3, 5);
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);

    }
    
    //Ali ako id nije isti i ako se stavi false posto ovakav ne postoji onda je GRESKA
    //Uvek vraca true
       @Test
    public void testDeleteDeliveryService2() {
        System.out.println("deleteDeliveryService");
        DeliveryService d = new DeliveryService(17, "FederalExp", 200, 200);
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);

    }
  
    /**
     * Test of updateInfo method, of class DeliveryServiceService.
     */
    @Test
    public void testUpdateInfo() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(2,"Fed Ex",4,9);
        String name = "Maxi";
        float startingPrice = 10;
        float pricePerKilometer = 50;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    // testUpdateInfo2 id je null
    @Test
    public void testUpdateInfo2() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(null,"FEDERAL",2,3);
        String name = "Nesto";
        float startingPrice = 5;
        float pricePerKilometer = 4;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
}
