/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Blizz
 */
public class TransactionServiceTest {
    
    public TransactionServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

  //Pocetak testiranja
   

    @Test
    public void testCompleteTransaction() {
        System.out.println("completeTransaction");
        Client c = new Client(3, "Oberon", "ob1", "ob2");
        DeliveryService d = new DeliveryService(3,"PolarExp", 7, 3);
        ShopItemService si = new ShopItemService();
        ShopItem s = new ShopItem(4, "Voda", 2, 205);
        int amount = 6;
        float distance = 6;
        TransactionService instance = new TransactionService(si);
        instance.completeTransaction(c, d, s, amount, distance);
        
        
    }
     @Test
    public void testCompleteTransaction2() {
         System.out.println("completeTransaction");
         Client c = new Client(4, "Titania", "t1", "t2");
         DeliveryService d = new DeliveryService(2, "Fed Ex", 4, 9);
         ShopItemService si = new ShopItemService();
         ShopItem s = new ShopItem(9, "Moka", 3, 5);
         int amount = 6;
         float distance = 6;
         TransactionService instance = new TransactionService(si);
         instance.completeTransaction(c, d, s, amount, distance);
        
       
        
    }
    
    // calculatePrice ne moze da se testira zato sto je metod Private 
    
    public void calculatePrice(){
        System.out.println("calculatePrice");
        ShopItem s = new ShopItem(8, "Kupus", 1, 5);
        DeliveryService d = new DeliveryService(4, "Federal", 2, 8);
        int amount = 5;
        float distance = 10;
        TransactionService instance = null;
        //instance.calculatePrice(s,distance,amount,d);//<--Gresku pokazuje ovde
        
    }

    /**
     * Test of getRecentTransactions method, of class TransactionService.
     */
    @Test
    public void testGetRecentTransactions() {
        System.out.println("getRecentTransactions");
        ShopItemService si = new ShopItemService();
        TransactionService instance = new TransactionService(si);
        ArrayList<Transaction> expResult = null;
        ArrayList<Transaction> result = instance.getRecentTransactions();
        assertEquals(expResult, result);
       
        
    }
    
}
